# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.upcase.chars.select.with_index {|letter,idx| letter == str[idx]}.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  length = str.length; a = length/2
  length.odd? ? str[a] : str[a-1..a]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count("[/aeiou/]")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  1.upto(num) { |i| result *= i }
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each_with_index do |el,idx|
    idx == arr.length-1 ? result += "#{el}" : result += "#{el}#{separator}"
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.chars.map.with_index { |i,idx| idx.even? ? i.downcase : i.upcase} .join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str.split(" ").map do |word|
    word.length > 4 ? word.reverse : word
  end.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  (1..n).to_a.map do |i|
    if i % 3 == 0 && i % 5 == 0
      i = "fizzbuzz"
    elsif i % 3 == 0
      i = "fizz"
    elsif i % 5 == 0
      i = "buzz"
    else
      i
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  #arr.reverse
  results = []
  arr.each {|i| results.unshift(i)}
  results
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1 || num == 0
  result = true
  2.upto(num/2) {|i| result = false if num % i == 0}
  result
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  return [1] if num == 1
  result = [1]
  2.upto(num/2) { |i| result << i if num % i == 0 }
  result << num
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |i| prime?(i) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  arr[0..2].count { |i| i.even? } > 1 ? even_check = true : even_check = false
  even_check ? arr.reject { |i| i.even? }[0] : arr.select { |i| i.even? }[0]
end
